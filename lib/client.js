const readline = require("readline");

const linereader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const run = () => {
  const game = {
    status: "RUNNING",
    lives: 2
  };
  console.log("**********************");
  console.log("*  N E W    G A M E  *");
  console.log("**********************");
  game_loop(game);
};

const game_loop = game => {
  printGameState(game);
  evaluateGuess(game);
};

const printGameState = game => {
  console.log("Remaining Lives: " + game.lives);
};

const evaluateGuess = game => {
  linereader.question("Guess a letter: ", letter => {
    game_loop(game);
  });
};

module.exports = {
  run
};
